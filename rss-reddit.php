<?php
header('Content-Type:text/json');
$filename = 'rss/' . date('Y-m-d-') . 'reddit.xml';
if (!file_exists($filename)) {
    file_put_contents($filename, file_get_contents('https://www.reddit.com/.rss'));
}
echo file_get_contents($filename);
