<?php
header('Content-Type:text/json');
$filename = 'rss/' . date('Y-m-d-') . 'sciencedaily.xml';
if (!file_exists($filename)) {
    file_put_contents($filename, file_get_contents('https://www.sciencedaily.com/rss/top/science.xml'));
}
echo file_get_contents($filename);
