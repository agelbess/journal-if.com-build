<?php
header('Content-Type:text/json');
$filename = 'rss/' . date('Y-m-d-') . 'eurekalert.xml';
if (!file_exists($filename)) {
    file_put_contents($filename, file_get_contents('https://www.eurekalert.org/rss.xml'));
}
echo file_get_contents($filename);
